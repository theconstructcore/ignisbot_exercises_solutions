#!/usr/bin/env python3
import os
import sys

# ROS imports
import rospy
import rospkg
from sensor_msgs.msg import Image

# trt_pose related imports
import json
import trt_pose.coco
import trt_pose.models
import torch

import time
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import torchvision.transforms as transforms
import PIL.Image
from trt_pose.draw_objects import DrawObjects
from trt_pose.parse_objects import ParseObjects
#from jetcam.utils import bgr8_to_jpeg

import numpy
numpy.set_printoptions(threshold=sys.maxsize)

# OpenPose objects
from openpose_ros_msgs.msg import PersonDetection, BodypartDetection

# Pickle utils
from pickle_commons import update_pickle_file, read_pickle_file

class IgnisPoepleTracker(object):

    def __init__(self, do_benchmark=True, device_to_use="cuda", csi_camera=True, plot_images=False, simulated_camera=False):
        """
        device_to_use: Can be "cpu" or "cuda". If you dont have cuda, then use cpu.
        csi_camera: If true we conside camera images come from CSI, otherwise its a USB camera. Assuming
        a Jetson Card is used.
        """

        rospy.loginfo("Starting IgnisPeopleTracker...")

        # Variable to avoid publisher errors when closing script.
        self._device_to_use = device_to_use
        self._plot_images = plot_images
        self._simulated_camera = simulated_camera

        rospy.loginfo("Starting PeopleTRacker ROS publisher")
        self.peopletrack_pub = rospy.Publisher('/ignis_peopletracker_ros/detected_poses_keypoints', PersonDetection, queue_size=1)

        rospy.loginfo("Starting Global PeopleDetection ROS publisher, represents mean in x and Y of all detections")
        self.global_detection_pub = rospy.Publisher('/ignis_peopletracker_ros/global_detection', BodypartDetection, queue_size=1)

        # Setup Shutdown hooks
        #rospy.on_shutdown(self.shutdown_hook)

        rospy.loginfo("Getting Path to package...")
        follow_people_configfiles_path = rospkg.RosPack().get_path('ignisbot_people_follower_pkg')+"/trt_config_files"

        # Path to optimised pickled model
        PKL_OPTIMIZED_MODEL = "optimised_model.pickle"
        pkl_optimized_model_weights_path = os.path.join(follow_people_configfiles_path, PKL_OPTIMIZED_MODEL)

        rospy.loginfo("We get the human pose json file that described the human pose")
        humanPose_file_path = os.path.join(follow_people_configfiles_path, 'human_pose.json')

        rospy.loginfo("Opening json file")
        with open(humanPose_file_path, 'r') as f:
            self.human_pose = json.load(f)

        rospy.loginfo("Creating topology")
        self.topology = trt_pose.coco.coco_category_to_topology(self.human_pose)
        rospy.logdebug("Topology====>"+str(self.topology))

        # We check if an optimised model was created already
        if os.path.isfile(pkl_optimized_model_weights_path):
            pass
        else:
            pass

        rospy.loginfo("# Load the model")
        num_parts = len(self.human_pose['keypoints'])
        num_links = len(self.human_pose['skeleton'])

        rospy.loginfo("Creating Model")
        if device_to_use == "cuda":
            model = trt_pose.models.resnet18_baseline_att(num_parts, 2 * num_links).cuda().eval()
        else:
            model = trt_pose.models.resnet18_baseline_att(num_parts, 2 * num_links).cpu().eval()

        rospy.loginfo("Load the weights from the eight files predownloaded to this package")
        MODEL_WEIGHTS = 'resnet18_baseline_att_224x224_A_epoch_249.pth'
        model_weights_path = os.path.join(follow_people_configfiles_path, MODEL_WEIGHTS)

        rospy.loginfo("Load state dict")
        if device_to_use == "cuda":
            model.load_state_dict(torch.load(model_weights_path))
        else:
            model.load_state_dict(torch.load(model_weights_path, map_location=torch.device('cpu')))

        self.WIDTH = 224
        self.HEIGHT = 224

        rospy.loginfo("Creating empty data")
        if device_to_use == "cuda":
            data = torch.zeros((1, 3, self.HEIGHT, self.WIDTH)).cuda()
        else:
            data = torch.zeros((1, 3, self.HEIGHT, self.WIDTH)).cpu()

        if device_to_use == "cuda":
            import torch2trt
            from torch2trt import TRTModule

            rospy.loginfo("Use tortchtrt to go from Torch to TensorRT to generate an optimised model")
            self.model_trt = torch2trt.torch2trt(model, [data], fp16_mode=True, max_workspace_size=1<<25)

            rospy.loginfo("We save that optimised model so that we dont have to do it everytime")
            OPTIMIZED_MODEL = 'resnet18_baseline_att_224x224_A_epoch_249_trt.pth'
            optimized_model_weights_path = os.path.join(follow_people_configfiles_path, OPTIMIZED_MODEL)

            rospy.loginfo("Saving new optimodel")
            torch.save(self.model_trt.state_dict(), optimized_model_weights_path)


            rospy.loginfo("We now load the saved model using Torchtrt")
            self.model_trt = TRTModule()
            self.model_trt.load_state_dict(torch.load(optimized_model_weights_path))

        else:
            # CPU ONLY
            self.model_trt = model



        rospy.loginfo("We benchmark the model")
        if do_benchmark:
            t0 = time.time()
            if device_to_use == "cuda":
                torch.cuda.current_stream().synchronize()
                for i in range(50):
                    y = self.model_trt(data)
                torch.cuda.current_stream().synchronize()
            else:
                torch.cpu.current_stream().synchronize()
                for i in range(50):
                    y = self.model_trt(data)
                torch.cpu.current_stream().synchronize()
            t1 = time.time()

            print(50.0 / (t1 - t0))

        rospy.loginfo("Define the Image processing variables")
        if device_to_use == "cuda":
            self.mean = torch.Tensor([0.485, 0.456, 0.406]).cuda()
            self.std = torch.Tensor([0.229, 0.224, 0.225]).cuda()
        else:
            self.mean = torch.Tensor([0.485, 0.456, 0.406]).cpu()
            self.std = torch.Tensor([0.229, 0.224, 0.225]).cpu()

        self.device = torch.device(self._device_to_use)

        rospy.loginfo("Clases to parse the obect of the NeuralNetwork and draw n the image")
        self.parse_objects = ParseObjects(self.topology)
        self.draw_objects = DrawObjects(self.topology)

        rospy.loginfo("Start setup camera")
        if not self._simulated_camera:
            if csi_camera:
                from jetcam.csi_camera import CSICamera
                self.camera = CSICamera(width=self.WIDTH, height=self.HEIGHT, capture_fps=30)
            else:
                from jetcam.usb_camera import USBCamera
                self.camera = USBCamera(width=self.WIDTH, height=self.HEIGHT, capture_fps=30)
        else:
            rospy.logerr("Simulated Camera")
            from cv_bridge import CvBridge, CvBridgeError

            self.cv_image = None

            self.bridge_object = CvBridge()
            self.image_sub = rospy.Subscriber("/jetdog/cam/image_raw",Image,self.camera_callback)


        rospy.loginfo("Starting IgnisPeopleTracker...DONE")

    def camera_callback(self,data):

        try:
            # We select bgr8 because its the OpneCV encoding by default
            self.cv_image = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)



    def get_width_height(self):
        return self.WIDTH, self.HEIGHT

    def __del__(self):
        self.shutdown_hook


    def shutdown_hook(self):

        print ("Shutdown time! HOOK ACTIVATED")
        print("Terminating, deleting objects...")
        self.camera.unobserve_all()
        print("Terminating, deleting objects...Cleanup finished")
        if self._plot_images:
            cv2.destroyAllWindows()
        print ("Shutdown time! HOOK ACTIVATED...DONE")

    def preprocess(self, image):
        self.device = torch.device(self._device_to_use)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = PIL.Image.fromarray(image)
        image = transforms.functional.to_tensor(image).to(self.device)
        image.sub_(self.mean[:, None, None]).div_(self.std[:, None, None])
        return image[None, ...]

    def execute(self, change):

        image = change['new']
        before_seconds = rospy.get_time()
        self.process_image(image)
        after_seconds = rospy.get_time()
        delta = after_seconds - before_seconds
        rospy.loginfo("GPU Processing Done, Seconds Time="+str(delta))

    def process_image(self, image):

        data = self.preprocess(image)
        cmap, paf = self.model_trt(data)
        cmap, paf = cmap.detach().cpu(), paf.detach().cpu()
        counts, objects, peaks = self.parse_objects(cmap, paf)#, cmap_threshold=0.15, link_threshold=0.15)
        # Draw New Image
        if self._plot_images:
            self.draw_objects(image, counts, objects, peaks)
            cv2.imshow("PeopleTracking", image)
            cv2.waitKey(1)

        self.publish_skeleton_markers(object_counts=counts,
                                        objects=objects,
                                        normalized_peaks=peaks,
                                        human_pose=self.human_pose)


    def start_peopletracker(self):
        rospy.loginfo("Starting Loop...")
        self.camera.running = True
        self.camera.observe(self.execute, names='value')
        rospy.spin()
        rospy.loginfo("Terminating Loop...")

    def start_sim_peopletracker(self):
        rospy.loginfo("Starting SIM Loop...")
        peopletracker_rate = rospy.Rate(5)
        while not rospy.is_shutdown():
            if self.cv_image is not None:
                rospy.loginfo("Processing Image...")
                before_seconds = rospy.get_time()
                self.process_image(self.cv_image)
                after_seconds = rospy.get_time()
                delta = after_seconds - before_seconds
                rospy.loginfo("CPU Processing Done, Seconds Time="+str(delta))
            peopletracker_rate.sleep()

        rospy.loginfo("Terminating Loop...")


    def publish_skeleton_markers(self, object_counts, objects, normalized_peaks, human_pose):

        # We start the PersonDetection object message
        peopletrack_msgs = PersonDetection()


        keypoints = human_pose['keypoints']
        skeleton = human_pose['skeleton']

        topology = self.topology
        height = self.HEIGHT
        width = self.WIDTH

        K = topology.shape[0]
        count = int(object_counts[0])
        peopletrack_msgs.num_people_detected = count

        K = topology.shape[0]

        # Start globa_x and y
        # This represents the mean of all the detections to simplify a person detection to a point
        global_detections_num = 0
        global_x = 0
        global_y = 0
        global_bodypart = BodypartDetection()

        for i in range(count):
            peopletrack_msgs.person_ID = i

            # TODO: Get correct order of joints and body parts
            color = (0, 255, 0)
            obj = objects[0][i]
            C = obj.shape[0]
            #rospy.loginfo("Object Shape==>"+str(C))
            #rospy.loginfo("Object ==>"+str(obj))
            for j in range(C):

                # j is the index of the object in humanpose keypoints
                body_part_name = keypoints[j]
                k = int(obj[j])
                #rospy.loginfo("BodyPart="+str(body_part_name)+", k ==>"+str(k))

                if k >= 0:
                    peak = normalized_peaks[0][j][k]
                    x = round(float(peak[1]) * width)
                    y = round(float(peak[0]) * height)

                    peopletrack_msgs = self.update_peopletrack_msgs(peopletrack_msgs,
                                                                    body_part_name,
                                                                    x,
                                                                    y)

                    global_x += x
                    global_y += y
                    global_detections_num += 1


            self.peopletrack_pub.publish(peopletrack_msgs)

        if global_detections_num > 0:
            global_bodypart.x = round(float(global_x) / global_detections_num)
            global_bodypart.y = round(float(global_y) / global_detections_num)

        self.global_detection_pub.publish(global_bodypart)


    def update_peopletrack_msgs(self, peopletrack_msgs, body_part_name, x, y):

        body_part_msg = BodypartDetection()
        body_part_msg.x = x
        body_part_msg.y = y
        # If we have the data the confidence is 100%
        body_part_msg.confidence = 1.0

        # Arms
        if body_part_name == "right_shoulder":
            peopletrack_msgs.right_shoulder = body_part_msg
        if body_part_name == "right_elbow":
            peopletrack_msgs.right_elbow = body_part_msg
        if body_part_name == "right_wrist":
            peopletrack_msgs.right_wrist = body_part_msg
        if body_part_name == "left_shoulder":
            peopletrack_msgs.left_shoulder = body_part_msg
        if body_part_name == "left_elbow":
            peopletrack_msgs.left_elbow = body_part_msg
        if body_part_name == "left_wrist":
            peopletrack_msgs.left_wrist = body_part_msg

        # Legs
        if body_part_name == "right_hip":
            peopletrack_msgs.right_hip = body_part_msg
        if body_part_name == "right_knee":
            peopletrack_msgs.right_knee = body_part_msg
        if body_part_name == "right_ankle":
            peopletrack_msgs.right_ankle = body_part_msg
        if body_part_name == "left_hip":
            peopletrack_msgs.left_hip = body_part_msg
        if body_part_name == "left_knee":
            peopletrack_msgs.left_knee = body_part_msg
        if body_part_name == "left_ankle":
            peopletrack_msgs.left_ankle = body_part_msg

        # Face
        if body_part_name == "nose":
            peopletrack_msgs.nose = body_part_msg
        if body_part_name == "neck":
            peopletrack_msgs.neck = body_part_msg
        if body_part_name == "right_eye":
            peopletrack_msgs.right_eye = body_part_msg
        if body_part_name == "left_eye":
            peopletrack_msgs.left_eye = body_part_msg
        if body_part_name == "right_ear":
            peopletrack_msgs.right_ear = body_part_msg
        if body_part_name == "left_ear":
            peopletrack_msgs.left_ear = body_part_msg



        return peopletrack_msgs


if __name__ == "__main__":
    rospy.init_node("peopletracker_test_note", log_level=rospy.INFO)
    if len(sys.argv) < 6:
        print("usage: peopletracker.py do_benchmark device_to_use csi_camera plot_images simulated_camera")
    else:
        do_benchmark = (sys.argv[1] == "true")
        # cpu or cuda
        device_to_use = sys.argv[2]
        csi_camera = (sys.argv[3] == "true")
        plot_images = (sys.argv[4] == "true")
        simulated_camera = (sys.argv[5] == "true")

    pt_object = IgnisPoepleTracker(do_benchmark=do_benchmark,
                                    device_to_use=device_to_use,
                                    csi_camera=csi_camera,
                                    plot_images=plot_images,
                                    simulated_camera=simulated_camera)
    if not simulated_camera:
        pt_object.start_peopletracker()
    else:
        pt_object.start_sim_peopletracker()
    print("Exiting script...")
