#!/usr/bin/env python3
import time
import math
import rospy
from std_msgs.msg import Float64

class TailPubWag(object):

    def __init__(self):
        self._tail_wag_hz_data = Float64()
        self._tail_wag_hz_data.data = 0.0
        self._tail_wag_pub_name = "/ignisbot/tailwag_freq"
        self.tail_wag_pub = rospy.Publisher(self._tail_wag_pub_name, Float64, queue_size=1)
        self._check_tail_wag_pub_connection()

    def _check_tail_wag_pub_connection(self):

        rate = rospy.Rate(10)  # 10hz
        while self.tail_wag_pub.get_num_connections() == 0 and not rospy.is_shutdown():
            rospy.logdebug("No susbribers to "+str(self._tail_wag_pub_name)+" yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("tail_wag_pub Publisher Connected")

    def tail_hz_update(self, hz_value):
        self._tail_wag_hz_data.data = hz_value
        self.tail_wag_pub.publish(self._tail_wag_hz_data)

    def start_loop(self):
        # To avoid overflow we use rate
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            self.tail_hz_update(hz_value=3.0)
            rate.sleep()

if __name__ == "__main__":
    rospy.init_node("ignisbot_tailwag_node", log_level=rospy.INFO)
    tailwag_obj = TailPubWag()
    tailwag_obj.start_loop()
